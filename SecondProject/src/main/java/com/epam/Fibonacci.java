/**
 * Package com.epam.
 */
package com.epam;

import static java.lang.Math.abs;

/**
 * The class "Fibonacci".
 */
class Fibonacci {
    /**
     * Fibonacci Biggest Odd.
     */
    private int fibonacciBiggestOdd = 1;
    /**
     * Fibonacci Biggest Even.
     */
    private int fibonacciBiggestEven = 0;
    /**
     * Sum Of Odd Fibonacci.
     */
    private int sumOfOddFibonacci = 0;
    /**
     * Sum Of Even Fibonacci.
     */
    private int sumOfEvenFibonacci = 0;
    /**
     * Hundred Percent Constant.
     */
    static final int hundredPercent = 100;

    /**
     * Create a range of Fibonacci numbers.
     *
     * @param TheSizeOfSet Is the size of Fibonacci numbers to create
     */
    public void createFibonacci(int TheSizeOfSet) {
        int[] arrayOfFibonacci = new int[TheSizeOfSet];
        arrayOfFibonacci[0] = 0;
        arrayOfFibonacci[1] = 1;
        for (int i = 2; i < TheSizeOfSet; i++) {
            arrayOfFibonacci[i] = (arrayOfFibonacci[i - 1] + arrayOfFibonacci[i - 2]);
        }
        for (int i = 0; i < TheSizeOfSet; i++) {
            if (arrayOfFibonacci[i] % 2 == 0) {
                sumOfEvenFibonacci += arrayOfFibonacci[i];
                if (fibonacciBiggestEven < arrayOfFibonacci[i]) {
                    fibonacciBiggestEven = arrayOfFibonacci[i];
                }
            } else {
                sumOfOddFibonacci += arrayOfFibonacci[i];
                if (fibonacciBiggestOdd < arrayOfFibonacci[i]) {
                    fibonacciBiggestOdd = arrayOfFibonacci[i];
                }
            }
        }
        System.out.println("FibonacciBiggestEven = " + fibonacciBiggestEven);
        System.out.println("FibonacciBiggestOdd = " + fibonacciBiggestOdd);

    }

    /**
     * Print to concole percentage of Fibonacci.
     * odd and even.
     */
    public void printToConcolePersentageOfFibonacciOddAndEven() {
        double percentageOfEven = ((sumOfOddFibonacci + sumOfEvenFibonacci) / sumOfEvenFibonacci);
        percentageOfEven = hundredPercent / percentageOfEven;
        double percentageOfOdd = abs(hundredPercent - percentageOfEven);
        System.out.println("PercentageOfOdd = " + percentageOfOdd);
        System.out.println("PercentageOfEven = " + percentageOfEven);
    }
}
