/**
 * Package com.epam.
 */
package com.epam;

/**
 * The class "Calculator".
 */
public class Calculator {
    /**
     * Begin Interval.
     */
    private int beginInterval;
    /**
     * End Interval.
     */
    private int endInterval;
    /**
     * Sum of odd numbers.
     */
    private int sumOfOddNumbers;
    /**
     * Sum of even numbers.
     */
    private int sumOfEvenNumbers;

    /**
     * Create a range of odd numbers.
     *
     * @param beginNumberInterval is the start of range
     * @param endNumberInterval   is the end of range
     */
    public void setNumberInterval(final int beginNumberInterval, final int endNumberInterval) {
        this.beginInterval = beginNumberInterval;
        this.endInterval = endNumberInterval;
    }

    /**
     * Returns the begin interval.
     *
     * @return Begin Interval
     */
    public int getBeginInterval() {
        return this.beginInterval;
    }

    /**
     * Returns the end interval.
     *
     * @return End Interval
     */
    public int getEndInterval() {
        return this.endInterval;
    }

    /**
     * Prints to concole odd numbers in both directions.
     *
     * @param theBeginingIndex is the start of range
     * @param theEndIndex      is the end of range
     */
    public void printToConcoleOddNumbersInBothDirections(int theBeginingIndex, int theEndIndex) {
        int tempArrayOfOddNumbers[] = new int[theEndIndex];
        int counterForTempArray = 0;
        for (int i = theBeginingIndex; i <= theEndIndex; i++) {
            if (i % 2 == 0) {
                sumOfEvenNumbers += i;
            } else {
                tempArrayOfOddNumbers[counterForTempArray] = i;
                counterForTempArray++;
            }
        }

        for (int i = 0; i <= counterForTempArray - 1; i++) {
            sumOfOddNumbers += tempArrayOfOddNumbers[i];
            System.out.print(tempArrayOfOddNumbers[i] + " ");
        }
        System.out.println();
        for (int i = counterForTempArray - 1; i >= 0; i--) {

            System.out.print(tempArrayOfOddNumbers[i] + " ");
        }

    }

    /**
     * Prints the sum of odd and even numbers.
     */
    public void printToConcoleTheSumOfOddAndEvenNumbers() {
        System.out.println();
        System.out.println("Sum of odd numbers: " + sumOfOddNumbers);
        System.out.println("Sum of even numbers: " + sumOfEvenNumbers);
        System.out.println();
    }
}
