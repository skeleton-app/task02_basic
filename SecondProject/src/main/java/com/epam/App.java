/**
 * Package com.epam.
 */
package com.epam;

import java.util.Scanner;

/**
 * The main class "Application".
 */
public class App {
    /**
     * Begin Interval Constant.
     */
    final static int beginIntervalConstant = 20;
    /**
     * End Interval Constant.
     */
    final static int endIntervalConstant = 80;

    /**
     * The main methode.
     *
     * @param args recieve text from concole
     */
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        Fibonacci fibonacci = new Fibonacci();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input begin of range: ");
        int beginInterval = scanner.nextInt();
        System.out.println("Input end of range: ");
        int endNumberInterval = scanner.nextInt();
        if (beginInterval > endNumberInterval) {
            System.out.println("Input Error,your range not correct!!! ");
            System.out.println("Setted default range: ");
            System.out.println("BeginInterval: 20 ");
            System.out.println("EndInterval: 80 ");

            beginInterval = beginIntervalConstant;
            endNumberInterval = endIntervalConstant;
        }
        calculator.setNumberInterval(beginInterval, endNumberInterval);
        calculator.printToConcoleOddNumbersInBothDirections(beginInterval, endNumberInterval);
        calculator.printToConcoleTheSumOfOddAndEvenNumbers();
        System.out.println("Input the amount of Fibonacci numbers: ");
        int fibonacciNumber = scanner.nextInt();
        fibonacci.createFibonacci(fibonacciNumber);
        fibonacci.printToConcolePersentageOfFibonacciOddAndEven();
    }
}
